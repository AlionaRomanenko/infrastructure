package com.university.infrastructure.repository;

import com.university.infrastructure.model.Client;
import com.university.infrastructure.model.Magnet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MagnetRepository extends JpaRepository<Magnet, Long> {
}
