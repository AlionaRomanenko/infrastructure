package com.university.infrastructure.repository;

import com.university.infrastructure.model.Bar;
import com.university.infrastructure.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    public List<Client> findAllByEmail(String email);
}
