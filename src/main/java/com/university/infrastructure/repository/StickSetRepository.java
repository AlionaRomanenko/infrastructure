package com.university.infrastructure.repository;

import com.university.infrastructure.model.StickSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StickSetRepository extends JpaRepository<StickSet,Long> {
}
