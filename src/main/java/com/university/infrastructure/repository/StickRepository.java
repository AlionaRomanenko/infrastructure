package com.university.infrastructure.repository;

import com.university.infrastructure.model.Stick;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StickRepository extends JpaRepository<Stick, Long> {
}
