package com.university.infrastructure.controller;

import com.university.infrastructure.DTO.OrderDTO;
import com.university.infrastructure.mapper.ProductOrderMapper;
import com.university.infrastructure.model.Client;
import com.university.infrastructure.model.ProductOrder;
import com.university.infrastructure.repository.ClientRepository;
import com.university.infrastructure.repository.ProductOrderRepository;
import com.university.infrastructure.service.ClientService;
import com.university.infrastructure.service.OrderService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProductOrderController {

    private final ProductOrderRepository productOrderRepository;
    private final OrderService orderService;
    private final ClientService clientService;
    private final ClientRepository clientRepository;
    private final ProductOrderMapper productOrderMapper;

    @Autowired
    public ProductOrderController(ProductOrderRepository productOrderRepository, OrderService orderService, ClientService clientService, ClientRepository clientRepository, ProductOrderMapper productOrderMapper) {
        this.productOrderRepository = productOrderRepository;
        this.orderService = orderService;
        this.clientService = clientService;
        this.clientRepository = clientRepository;
        this.productOrderMapper = Mappers.getMapper(ProductOrderMapper.class );
    }

    @PostMapping("/api/createOrder")
    public ResponseEntity addOrder(@Valid @RequestBody OrderDTO order, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
        }

        if (clientService.clientExists(order.getEmail())){
            Client client = new Client();
            client.setEmail(order.getEmail());
            client.setFirstName(order.getFirstName());
            client.setLastName(order.getLastName());
            client.setPhoneNumber(order.getPhoneNumber());
            clientRepository.save(client);
        }
        ProductOrder prod = productOrderMapper.orderDTOToProductOrder(order);
        productOrderRepository.save(prod);


        return ResponseEntity.ok().build();
    }



    @GetMapping("/api/admin/orders")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<ProductOrder>> retrieveOrders(){
        return ResponseEntity.ok(productOrderRepository.findAll());
    }

}
