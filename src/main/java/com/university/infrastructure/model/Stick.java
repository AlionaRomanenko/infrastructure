package com.university.infrastructure.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Stick implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer weight;

    @Column
    private String colour;

    @Column
    private String type;

    @ManyToOne
    @JoinColumn(name="stickSet_id")
    private StickSet stickSet;
}
