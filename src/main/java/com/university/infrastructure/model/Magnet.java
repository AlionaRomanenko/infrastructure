package com.university.infrastructure.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Magnet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer diameter;

    @OneToOne
    @JoinColumn(name="product_id")
    private Product product;
}
