package com.university.infrastructure.model;

import lombok.Data;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class ProductOrder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private Long number;

    @Column
    private LocalDateTime date = LocalDateTime.now();

    @Column
    private Boolean sendType;

    @Column
    private Boolean payType;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @OneToMany(mappedBy = "productOrder", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Product> products = new ArrayList<>();

}
