package com.university.infrastructure.mapper;

import com.university.infrastructure.DTO.OrderDTO;
import com.university.infrastructure.model.ProductOrder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductOrderMapper {
    ProductOrder orderDTOToProductOrder(OrderDTO orderDTO);
}
