package com.university.infrastructure.service;

import com.university.infrastructure.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }


    public boolean clientExists(String email){
        return !clientRepository.findAllByEmail(email).isEmpty();
    }
}
